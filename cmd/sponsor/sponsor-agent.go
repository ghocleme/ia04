package main

import (
    "ia04/agt"
    "ia04/agt/sponsoragent"
    "strconv"
    "time"
)

const URL = "http://localhost:12000"

func main() {

    i := 1

    // In an infinite loop, creating a new sponsor every 15 seconds.
    // Each sponsor will create a ballot, wait for its result and then delete it.
    for {

        id := agt.AgentID("sponsor-" + strconv.Itoa(i))

        agent := sponsoragent.NewSponsorAgent(id, URL)
        agent.Start()

        time.Sleep(15 * time.Second)

        i++
    }
}