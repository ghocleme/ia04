package main

import (
    "fmt"
    "ia04/agt/ballotagent"
)

func main() {

    agt := ballotagent.NewBallotAgent("ballot-agt1", ":12000")
    agt.Start()

    fmt.Scanln()
}