package main

import (
    "fmt"
    "ia04/agt"
    "ia04/agt/voteragent"
    "strconv"
)

const URL = "http://localhost:12000"

func main() {

    for i := 1; i <= agt.NB_VOTERS; i++ {

        id := agt.AgentID("ag_id" + strconv.Itoa(i))

        agent := voteragent.NewVoterAgent(id, URL)
        agent.Start()
    }

    fmt.Scanln()
}
