package agt

import "ia04/comsoc"

func Contains[T comparable](sl []T, val T) (int, bool) {

    for i, v := range sl {

        if v == val {
            return i, true
        }
    }
    return -1, false
}

func HasDuplicates[T comparable](sl []T) bool {

    for i, v := range sl {

        for j := i+1; j < len(sl); j++ {

            if v == sl[j] {
                return true
            }
        }
    }
    return false
}

// MakeRange Creates a slice of alternatives.
func MakeRange(min, max int) []comsoc.Alternative {
    a := make([]comsoc.Alternative, max-min+1)
    for i := range a {
        a[i] = comsoc.Alternative(min + i)
    }
    return a
}
