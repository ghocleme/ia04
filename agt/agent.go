package agt

type AgentI interface {
    Equal(ag AgentI) bool
    DeepEqual(ag AgentI) bool
    Clone() AgentI
    String() string
    Start()
}

