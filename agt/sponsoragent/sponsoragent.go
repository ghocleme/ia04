package sponsoragent

import (
    "bytes"
    "encoding/json"
    "fmt"
    "ia04/agt"
    "log"
    "math/rand"
    "net/http"
    "strconv"
    "time"
)

const BALLOT_DURATION = 30

type SponsorAgent struct {
    id      agt.AgentID
    url string
    ballot agt.Ballot
}

// NewSponsorAgent Create a new SponsorAgent.
func NewSponsorAgent(id agt.AgentID, url string) *SponsorAgent {
    return &SponsorAgent{
        id: id,
        url: url,
    }
}

// postBallot Submit a ballot.
func (agent *SponsorAgent) postBallot(ballot *agt.JsonBallot) *agt.Ballot {

    errorPrefix := "An error occurred when creating a new ballot:"

    url := agent.url + "/new_ballot"
    data, err := json.Marshal(ballot)

    // Check if an error occurred while marshaling object to json.
    if err != nil {
        agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
        return nil
    }

    // Sending the request.
    resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

    // Check if an error occurred with the request.
    if err != nil {
        agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
        return nil
    }

    // Parsing the response when the statut is not 201.
    if resp.StatusCode != http.StatusCreated {

        errorMsg := agt.JsonError{}
        err = agent.parseResponse(resp, &errorMsg)

        if err != nil {
            agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
        } else {
            agent.log(fmt.Sprintf("%s %s", errorPrefix, errorMsg.Error))
        }

        return nil
    }

    // Parsing the response when the statut is 201 (= ballot created).
    ballotId := &agt.JsonBallotId{}
    err = agent.parseResponse(resp, &ballotId)

    if err != nil {
        agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
        return nil
    }

    deadline, _ := time.Parse(agt.UTC_TIME_LAYOUT, ballot.Deadline)

    return agt.NewBallot(ballotId.BallotId, ballot.Rule, deadline, ballot.VoterIds, ballot.Alts)
}

// getResult Get the result of a ballot given its id.
func (agent *SponsorAgent) getResult(ballotId *agt.JsonBallotId) *agt.BallotResult {

    errorPrefix := "An error occurred when retrieving the result of the ballot:"

    url := agent.url + "/result"
    data, err := json.Marshal(ballotId)

    // Check if an error occurred while marshaling object to json.
    if err != nil {
        agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
        return nil
    }

    // Sending the request.
    resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

    // Check if an error occurred with the request.
    if err != nil {
        agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
        return nil
    }

    // Parsing the response when the statut is not 200.
    if resp.StatusCode != http.StatusOK {

        errorMsg := agt.JsonError{}
        err = agent.parseResponse(resp, &errorMsg)

        if err != nil {
            agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
        } else {
            agent.log(fmt.Sprintf("%s %s", errorPrefix, errorMsg.Error))
        }

        return nil
    }

    // Parsing the response when the statut is 200 (= result retrived).
    ballotResult := &agt.BallotResult{}
    err = agent.parseResponse(resp, &ballotResult)

    if err != nil {
        agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
        return nil
    }

    return ballotResult
}

// deleteBallot Delete a ballot given its id.
func (agent *SponsorAgent) deleteBallot(ballotId *agt.JsonBallotId) bool {

    errorPrefix := "An error occurred when retrieving the result of the ballot:"

    url := agent.url + "/del_ballot"
    data, err := json.Marshal(ballotId)

    // Check if an error occurred while marshaling object to json.
    if err != nil {
        agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
        return false
    }

    // Sending the request.
    resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

    // Check if an error occurred with the request.
    if err != nil {
        agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
        return false
    }

    // Parsing the response when the statut is not 200.
    if resp.StatusCode != http.StatusOK {

        errorMsg := agt.JsonError{}
        err = agent.parseResponse(resp, &errorMsg)

        if err != nil {
            agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
        } else {
            agent.log(fmt.Sprintf("%s %s", errorPrefix, errorMsg.Error))
        }

        return false
    }

    return true
}

// parseResponse Parse the response from an HTTP request into an internal object.
func (agent *SponsorAgent) parseResponse(resp *http.Response, data interface{}) error {

    body := resp.Body

    defer body.Close()

    buf := new(bytes.Buffer)
    _, err := buf.ReadFrom(resp.Body)

    if err != nil {
        return err
    }

    err = json.Unmarshal(buf.Bytes(), data)

    return err
}

func (agent *SponsorAgent) log(message string) {
    log.Printf("[%s] %s\n", agent.id, message)
}

func (agent *SponsorAgent) generateRandomBallot() *agt.JsonBallot {

    rule := agt.GetRandomRule()
    deadline := time.Now().Add(BALLOT_DURATION * time.Second).UTC().Format(agt.UTC_TIME_LAYOUT)
    voterIds := agent.generateRandomVoterIds()
    alts := agt.NB_ALTS

    return &agt.JsonBallot{Rule: rule, Deadline: deadline, VoterIds: voterIds, Alts: alts}
}

func (agent *SponsorAgent) generateRandomVoterIds() []agt.AgentID {

    rand.Seed(time.Now().UnixNano())
    n := 2 + rand.Intn(agt.NB_VOTERS)

    voterIds := make([]agt.AgentID, n)

    for i := range voterIds {
        voterIds[i] = agt.AgentID("ag_id" + strconv.Itoa(i+1))
    }

    return voterIds
}

func (agent *SponsorAgent) Start() {

    go func() {

        /*
        Creating a new ballot.
        */

        jsonBallot := agent.generateRandomBallot()
        ballot := agent.postBallot(jsonBallot)

        // Case when ballot cannot be created.
        if ballot == nil {
            return
        }

        agent.ballot = *ballot

        agent.log(fmt.Sprintf("Ballot '%s' has been created: rule=%s, deadline=%s, alts=%d",
            ballot.GetId(), ballot.GetRule(), ballot.GetDeadline().String(), ballot.GetAlts()))

        // Waiting for the ballot to be finished.
        time.Sleep(time.Second * (BALLOT_DURATION + 1))

        /*
        Retrieving the result of the ballot.
        */
        jsonBallotId := &agt.JsonBallotId{BallotId: ballot.GetId()}
        result := agent.getResult(jsonBallotId)

        // Case when there is no voter for the ballot.
        if result != nil {

            agent.log(fmt.Sprintf("Result received for ballot '%s': rule= %s, winner=%d, ranking=%v.",
                ballot.GetId(), ballot.GetRule(), result.Winner, result.Ranking))
        }

        /*
        Removing the ballot.
        */
        time.Sleep(15 * time.Second)

        deleted := agent.deleteBallot(jsonBallotId)

        if !deleted {
            return
        }

        agent.log(fmt.Sprintf("Ballot '%s' has been deleted.", ballot.GetId()))
    }()

    log.Printf("Sponsor '%s' started.\n", agent.id)
}

// String Convert a SponsorAgent to string.
func (agent *SponsorAgent) String() string {
    return fmt.Sprintf("SponsorAgent { agent-id=%s, url=%s, ballot=%s }", agent.id, agent.url, agent.ballot.String())
}

// Equal Check if a SponsorAgent is equal to another.
func (agent *SponsorAgent) Equal(compared agt.AgentI) bool {

    casted, ok := compared.(*SponsorAgent)

    if !ok {
        return false
    }

    if agent.id != casted.id {
        return false
    }

    if agent.url != casted.url {
        return false
    }

    if !agent.ballot.Equal(casted.ballot) {
        return false
    }

    return true
}

// DeepEqual Check if a SponsorAgent is deep equal to another.
func (agent *SponsorAgent) DeepEqual(compared agt.AgentI) bool {

    casted, ok := compared.(*SponsorAgent)

    if !ok {
        return false
    }

    if agent.id != casted.id {
        return false
    }

    if agent.url != casted.url {
        return false
    }

    if !agent.ballot.DeepEqual(casted.ballot) {
        return false
    }

    return true
}

// Clone Clone a SponsorAgent.
func (agent *SponsorAgent) Clone() agt.AgentI {

    cloned := SponsorAgent{}
    cloned.id = agent.id
    cloned.url = agent.url
    cloned.ballot = agent.ballot.Clone()

    return &cloned
}