package agt

import (
    "errors"
    "ia04/comsoc"
)

/*
JsonError Object used in requests to encapsulate an error message.
*/
type JsonError struct {
    Error string `json:"error"`
}

/*
JsonVoter Object used in requests to represent a voter id.
*/
type JsonVoter struct {
    VoterId AgentID `json:"agent-id"`
}


// IsComplete Check if all the fields of the structure are filled.
func (voter *JsonVoter) IsComplete() error {

    if voter.VoterId == "" {
        return errors.New("missing 'agent-id' or field is empty'")
    }

    return nil
}

/*
JsonBallotId Object used in requests to represent a ballot id.
*/
type JsonBallotId struct {
    BallotId string `json:"ballot-id"`
}

// IsComplete Check if all the fields of the structure are filled.
func (ballot *JsonBallotId) IsComplete() error {

    if ballot.BallotId == "" {
        return errors.New("missing 'ballot-id' or field is empty'")
    }

    return nil
}

/*
JsonVote Object used in requests to represent a submitted vote.
*/
type JsonVote struct {
    AgentId AgentID       `json:"agent-id"`
    VoteId  string        `json:"vote-id"`
    Prefs   []comsoc.Alternative `json:"prefs"`
    Options []int         `json:"options"`
}

// IsComplete Check if all the fields of the structure are filled.
func (vote JsonVote) IsComplete() error {

    if vote.AgentId == "" {
        return errors.New("missing 'agent-id or field is empty'")
    }

    if vote.VoteId == "" {
        return errors.New("missing 'vote-id or field is empty'")
    }

    if len(vote.Prefs) == 0 {
        return errors.New("missing 'agent-id or field is empty'")
    }

    return nil
}

/*
JsonVoterBallot Object used in requests to represent the available votes for a voter.
*/
type JsonVoterBallot struct {
    BallotId string `json:"ballot-id"`
    Rule     string `json:"rule"`
    Deadline string `json:"deadline"`
    Alts     int    `json:"#alts"`
    Voted    bool   `json:"has-voted"`
}

/*
JsonBallot Object used in requests to represent a ballot.
*/
type JsonBallot struct {
    BallotId string    `json:"ballot-id"`
    Rule     string    `json:"rule"`
    Deadline string    `json:"deadline"`
    VoterIds []AgentID `json:"voter-ids"`
    Alts     int       `json:"#alts"`
}

/*
IsComplete Check if all the fields of the structure are filled.
*/
func (ballot *JsonBallot) IsComplete() error {

    if ballot.Rule == "" {
        return errors.New("missing 'Rule' field or field is empty")
    }

    if ballot.Deadline == "" {
        return errors.New("missing 'Deadline' field or field is empty")
    }

    if len(ballot.VoterIds) == 0 {
        return errors.New("missing 'VoterIds' field or array is empty")
    }

    if ballot.Alts < 1 {
        return errors.New("missing '#Alts' or invalid value")
    }

    return nil
}