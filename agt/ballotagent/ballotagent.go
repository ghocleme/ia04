package ballotagent

import (
    "bytes"
    "encoding/json"
    "fmt"
    "ia04/agt"
    "log"
    "net/http"
	"sync"
	"time"
)

type BallotAgent struct {
	sync.Mutex
	id      agt.AgentID
	address string
    handler agt.BallotHandler
}

func NewBallotAgent(id agt.AgentID, address string) *BallotAgent {
    handler := *agt.NewBallotHandler()
    return &BallotAgent{
        id: id,
        address: address,
        handler: handler,
    }
}

/*
postBallot Create a new ballot.
*/
func (agent* BallotAgent) postBallot(writer http.ResponseWriter, request *http.Request) {

    // Check that the method is POST.
    if request.Method != http.MethodPost {
        agent.sendError(writer, http.StatusMethodNotAllowed, "invalid method")
        return
    }

    postBallot := agt.JsonBallot{}

    err := agent.decodeRequest(request, &postBallot)

    // Handle Unmarshal errors.
    if err != nil {
        agent.sendError(writer, http.StatusBadRequest, err.Error())
        return
    }

    // Check that the request is valid.
    if err = postBallot.IsComplete(); err != nil {
        agent.sendError(writer, http.StatusBadRequest, err.Error())
        return
    }

    // Check that the rule is available.
    if ok := agt.RuleExists(postBallot.Rule); !ok {
        agent.sendError(writer, http.StatusNotImplemented, "rule not implemented")
        return
    }

    deadline, err := time.Parse(agt.UTC_TIME_LAYOUT, postBallot.Deadline)

    // Handle parse errors and check that the deadline is in the future.
    if err != nil || deadline.Before(time.Now()) {
        agent.sendError(writer, http.StatusBadRequest, "invalid deadline, check format or that it is in the future")
        return
    }

    // Adding the ballot.
    agent.Lock() // Accessing critical resource so a lock is necessary.

    ballotId := agent.handler.NextId()
    ballot := agt.NewBallot(ballotId, postBallot.Rule, deadline, postBallot.VoterIds, postBallot.Alts)
    agent.handler.AddBallot(ballot)

    agent.Unlock() // Releasing the mutex.

    // Response.
    agent.sendResponse(writer, http.StatusCreated, map[string]string{"ballot-id": ballotId})

    // Log.
    log.Printf("[%s] Ballot '%s' created: rule=%s, deadline=%s, alts=%d, voters=%v\n",
        agent.id, ballotId, ballot.GetRule(), ballot.GetDeadline().String(), ballot.GetAlts(), ballot.GetVoterIds())
}

/*
getBallots Get all the available ballots for a voter.
*/
func (agent *BallotAgent) getBallots(writer http.ResponseWriter, request *http.Request) {

    // Check that the method is POST.
    if request.Method != http.MethodPost {
        agent.sendError(writer, http.StatusMethodNotAllowed, "invalid method")
        return
    }

    voterId := agt.JsonVoter{}
    err := agent.decodeRequest(request, &voterId)

    // Handle Unmarshal errors.
    if err != nil {
        agent.sendError(writer, http.StatusBadRequest, err.Error())
        return
    }

    err = voterId.IsComplete()

    // Handle parse errors
    if err != nil {
        agent.sendError(writer, http.StatusBadRequest, err.Error())
        return
    }

    // Retrieving all the ballots for which the agent can vote.
    ballots := agent.handler.GetBallots()
    voterBallots := make([]agt.JsonVoterBallot, 0)

    for _, ballot := range ballots {

        if ballot.CanVote(voterId.VoterId) {

            voterBallot := agt.JsonVoterBallot{
                BallotId: ballot.GetId(),
                Rule: ballot.GetRule(),
                Deadline: ballot.GetDeadline().Format(agt.UTC_TIME_LAYOUT),
                Alts: ballot.GetAlts(),
                Voted: ballot.HasVoted(voterId.VoterId),
            }

            voterBallots = append(voterBallots, voterBallot)
        }
    }

    // Response.
    agent.sendResponse(writer, http.StatusOK, voterBallots)

    // Log.
    log.Printf("[%s] Available ballots sent to agent '%s'.\n", agent.id, voterId.VoterId)
}

// postVote Submit a vote.
func (agent *BallotAgent) postVote(writer http.ResponseWriter, request *http.Request) {

    // Check that the method is POST.
    if request.Method != http.MethodPost {
        agent.sendError(writer, http.StatusMethodNotAllowed, "invalid method")
        return
    }

    pVote := agt.JsonVote{}
    err := agent.decodeRequest(request, &pVote)

    // Handle Unmarshal errors.
    if err != nil {
        agent.sendError(writer, http.StatusBadRequest, err.Error())
        return
    }

    err = pVote.IsComplete()

    // Handle parse errors
    if err != nil {
        agent.sendError(writer, http.StatusBadRequest, err.Error())
        return
    }

    // Check that the given ballot exists.
    if !agent.handler.HasBallot(pVote.VoteId) {
        agent.sendError(writer, http.StatusNotImplemented, "ballot not found")
        return
    }

    vote := agt.NewVote(pVote.AgentId, pVote.VoteId, pVote.Prefs, pVote.Options)
    _, ballot := agent.handler.GetBallot(pVote.VoteId)

    // Check that the agent can vote in this ballot.
    if !ballot.CanVote(pVote.AgentId) {
        agent.sendError(writer, http.StatusForbidden, "agent cannot vote in this ballot")
        return
    }

    // Check that the agent did not already pVote.
    if ballot.HasVoted(pVote.AgentId) {
        agent.sendError(writer, http.StatusForbidden, "agent has already voted")
        return
    }

    // Check that the ballot is not finished.
    if ballot.IsFinished() {
        agent.sendError(writer, http.StatusServiceUnavailable, "ballot is finished")
        return
    }

    // Check that the vote is valid.
    if !ballot.IsValid(vote) {
        agent.sendError(writer, http.StatusBadRequest, "vote is invalid, check alternatives")
        return
    }

    agent.Lock()
    err = ballot.AddVote(vote)
    agent.Unlock()

    // Should not happend because all the verifications have been done before.
    if err != nil {
        agent.sendError(writer, http.StatusInternalServerError, "an internal error occurred")
        return
    }

    // Response.
    writer.WriteHeader(http.StatusOK)

    // Log.
    log.Printf("[%s] New vote added for ballot '%s' by agent '%s'.\n", agent.id, ballot.GetId(), pVote.AgentId)
}

// getResult Get the result of a vote.
func (agent *BallotAgent) getResult(writer http.ResponseWriter, request *http.Request) {

    // Check that the method is POST.
    if request.Method != http.MethodPost {
        agent.sendError(writer, http.StatusMethodNotAllowed, "invalid method")
        return
    }

    ballotId := agt.JsonBallotId{}
    err := agent.decodeRequest(request, &ballotId)

    // Handle Unmarshal errors.
    if err != nil {
        agent.sendError(writer, http.StatusBadRequest, err.Error())
        return
    }

    err = ballotId.IsComplete()

    // Handle parse errors.
    if err != nil {
        agent.sendError(writer, http.StatusBadRequest, err.Error())
        return
    }

    // Check that the ballot exists.
    if !agent.handler.HasBallot(ballotId.BallotId) {
        agent.sendError(writer, http.StatusNotFound, "ballot not found")
        return
    }

    _, ballot := agent.handler.GetBallot(ballotId.BallotId)

    // Check that the vote is finished.
    if !ballot.IsFinished() {
        agent.sendError(writer, http.StatusTooEarly, "ballot is not finished")
        return
    }

    // Result computation.
    result := ballot.GetResult()

    // This may happend if the ballot has no vote (empty profile).
    if result == nil {
        agent.sendError(writer, http.StatusUnprocessableEntity, "no result can be computed as ballot has no vote")
        return
    }

    // Response.
    agent.sendResponse(writer, http.StatusOK, *result)

    // Log.
    log.Printf("[%s] Result sent for ballot %s.\n", agent.id, ballot.GetId())
}

func (agent *BallotAgent) deleteBallot(writer http.ResponseWriter, request *http.Request) {

    // Check that the method is POST.
    if request.Method != http.MethodPost {
        agent.sendError(writer, http.StatusMethodNotAllowed, "invalid method")
        return
    }

    ballotId := agt.JsonBallotId{}
    err := agent.decodeRequest(request, &ballotId)

    // Handle Unmarshal errors.
    if err != nil {
        agent.sendError(writer, http.StatusBadRequest, err.Error())
        return
    }

    err = ballotId.IsComplete()

    // Handle parse errors.
    if err != nil {
        agent.sendError(writer, http.StatusBadRequest, err.Error())
        return
    }

    // Check that the ballot exists.
    if !agent.handler.HasBallot(ballotId.BallotId) {
        agent.sendError(writer, http.StatusNotFound, "ballot not found")
        return
    }

    i, ballot := agent.handler.GetBallot(ballotId.BallotId)

    // Check that the vote is finished.
    if !ballot.IsFinished() {
        agent.sendError(writer, http.StatusTooEarly, "ballot is not finished")
        return
    }

    agent.Lock()
    agent.handler.RemoveBallot(i)
    agent.Unlock()

    // Response.
    writer.WriteHeader(http.StatusOK)

    // Log.
    log.Printf("[%s] Ballot %s has been deleted.\n", agent.id, ballot.GetId())
}

// sendError Send an error given a status and a message.
func (agent* BallotAgent) sendError(writer http.ResponseWriter, status int, message string) {
    data := agt.JsonError{Error: message}
    agent.sendResponse(writer, status, data)
}


// sendResponse Send a response given a status and data.
func (agent* BallotAgent) sendResponse(writer http.ResponseWriter, status int, data interface{}) {

    encoded, _ := json.Marshal(data)

    writer.Header().Add("Content-Type", "application/json")
    writer.WriteHeader(status)
    
    _, err := writer.Write(encoded)

    if err != nil {
        log.Println("An error occurred when sending a response.")
    }
}


// decodeRequest Decode an incoming request.
func (agent *BallotAgent) decodeRequest(request *http.Request, data interface{}) error {

    buf := new(bytes.Buffer)
    _, err := buf.ReadFrom(request.Body)

    if err != nil {
        return err
    }

    err = json.Unmarshal(buf.Bytes(), &data)

    return err
}


// Start Start the server.
func (agent *BallotAgent) Start() {

	mux := http.NewServeMux()
    mux.HandleFunc("/new_ballot", agent.postBallot)
    mux.HandleFunc("/del_ballot", agent.deleteBallot)
    mux.HandleFunc("/ballots", agent.getBallots)
    mux.HandleFunc("/vote", agent.postVote)
    mux.HandleFunc("/result", agent.getResult)

	server := http.Server{
		Addr:           agent.address,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

    log.Println("Starting a server on", agent.address)

	go log.Fatal(server.ListenAndServe())
}

// String Convert a BallotAgent to string.
func (agent *BallotAgent) String() string {
    return fmt.Sprintf("BallotAgent { id=%s, address=%s, handler=%v }", agent.id, agent.address, &agent.handler)
}

// Equal Check if a BallotAgent is equal to another.
func (agent *BallotAgent) Equal(compared agt.AgentI) bool {

    casted, ok := compared.(*BallotAgent)

    if !ok {
        return false
    }

    if agent.id != casted.id {
        return false
    }

    if agent.address != casted.address {
        return false
    }

    if !agent.handler.Equal(casted.handler) {
        return false
    }

    return true
}

// DeepEqual Check if a BallotAgent is deep equal to another.
func (agent *BallotAgent) DeepEqual(compared agt.AgentI) bool {

    casted, ok := compared.(*BallotAgent)

    if !ok {
        return false
    }

    if agent.id != casted.id {
        return false
    }

    if agent.address != casted.address {
        return false
    }

    if !agent.handler.DeepEqual(casted.handler) {
        return false
    }

    return true
}

// Clone Clone a BallotAgent.
func (agent *BallotAgent) Clone() agt.AgentI {

    cloned := BallotAgent{}
    cloned.id = agent.id
    cloned.address = agent.address
    cloned.handler = agent.handler.Clone()

    return &cloned
}