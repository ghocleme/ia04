package voteragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"ia04/agt"
	"ia04/comsoc"
	"log"
	"math/rand"
	"net/http"
	"time"
)

type VoterAgent struct {
	id  agt.AgentID
	url string
}

func NewVoterAgent(id agt.AgentID, url string) *VoterAgent {
	return &VoterAgent{
		id:  id,
		url: url,
	}
}

// parseResponse Parse the response from an HTTP request into an internal object.
func (agent *VoterAgent) parseResponse(resp *http.Response, data interface{}) error {

	body := resp.Body

	defer body.Close()

	buf := new(bytes.Buffer)
	_, err := buf.ReadFrom(resp.Body)

	if err != nil {
		return err
	}

	err = json.Unmarshal(buf.Bytes(), data)

	return err
}

// getBallots Get all the ballots available for a voter.
func (agent *VoterAgent) getBallots() []agt.JsonVoterBallot {

	errorPrefix := "An error occurred when getting the ballots:"

	url := agent.url + "/ballots"
	data, err := json.Marshal(agt.JsonVoter{VoterId: agent.id})

	// Check if an error occurred while marshaling object to json.
	if err != nil {
		agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
		return nil
	}

	// Sending the request.
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
		return nil
	}

	// Parsing the response when the statut is not 200 (status OK).
	if resp.StatusCode != http.StatusOK {

		errorMsg := agt.JsonError{}
		err = agent.parseResponse(resp, &errorMsg)

		if err != nil {
			agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
		} else {
			agent.log(fmt.Sprintf("%s %s", errorPrefix, errorMsg.Error))
		}

		return nil
	}

	if err != nil {
		agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
		return nil
	}

	var post []agt.JsonVoterBallot
	err = agent.parseResponse(resp, &post)

	if err != nil {
		agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
		return nil
	}

	return post
}

// sendVote Post request to send a vote.
func (agent *VoterAgent) sendVote(voteId string, prefs []comsoc.Alternative, options []int) error {

	errorPrefix := "An error occurred when sending a vote:"
	url := agent.url + "/vote"

	data, err := json.Marshal(agt.JsonVote{
		AgentId: agent.id,
		VoteId:  voteId,
		Prefs:   prefs,
		Options: options,
	})

	// Check if an error occurred while marshaling object to json.
	if err != nil {
		agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
		return err
	}

	// Sending the request.
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	if err != nil {
		agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
		return err
	}

	// Parsing the response when the statut is not 200 (status OK).
	if resp.StatusCode != http.StatusOK {

		errorMsg := agt.JsonError{}
		err = agent.parseResponse(resp, &errorMsg)

		if err != nil {
			agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
		} else {
			agent.log(fmt.Sprintf("%s %s", errorPrefix, errorMsg.Error))
		}
		return err
	}

	if err != nil {
		agent.log(fmt.Sprintf("%s %s", errorPrefix, err.Error()))
		return err
	}

	return nil
}

func (agent *VoterAgent) log(message string) {
	log.Printf("[%s] %s\n", agent.id, message)
}

// generate preferences and send vote for all the ballots of the voter every 5 seconds if possible
func (agent *VoterAgent) vote(ballots []agt.JsonVoterBallot) {

	for _, ballot := range ballots {

		deadline, _ := time.Parse(agt.UTC_TIME_LAYOUT, ballot.Deadline)

		// Taking the first non-voted and non finished ballot.
		if !ballot.Voted && deadline.After(time.Now().UTC()) {

			prefs := agt.MakeRange(1, ballot.Alts)
			prefs = agent.getPreferences(prefs)
			options := agent.getOptions(ballot.Rule, ballot.Alts)

			err := agent.sendVote(ballot.BallotId, prefs, options)

			if err == nil {
				agent.log(fmt.Sprintf("Vote sent: ballot-id=%s, rule=%s, prefs=%v, options=%v",
					ballot.BallotId, ballot.Rule, prefs, options))
			}

			return
		}
	}
}

// input ordered list 1 to n output a preference array
func (agent *VoterAgent) getPreferences(list []comsoc.Alternative) []comsoc.Alternative {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(list), func(i, j int) { list[i], list[j] = list[j], list[i] })
	return list
}

func (agent *VoterAgent) getOptions(rule string, alts int) []int {
	if rule == "approval" {
		options := make([]int, 1)
		rand.Seed(time.Now().UnixNano())
		options[0] = rand.Intn(alts-1) + 1
		return options
	}
	return nil
}

func (agent *VoterAgent) Start() {

	go func() {

		agent.log(fmt.Sprintf("Voter '%s' has been created.", agent.id))

		for {

			// Retrieving available ballots for the voter.
			ballots := agent.getBallots()

			// Voting.
			agent.vote(ballots)

			time.Sleep(5 * time.Second)
		}
	}()
}

// Clone a VoterAgent.
func (agent *VoterAgent) Clone() agt.AgentI {
    cloned := VoterAgent{}
    cloned.id = agent.id
    cloned.url = agent.url

    return &cloned
}

// String Convert a VoterAgent to string.
func (agent *VoterAgent) String() string {
    return fmt.Sprintf("VoterAgent { agent-id=%s, url=%s }", agent.id, agent.url)
}

// Equal Check if a VoterAgent is equal to another.
func (agent *VoterAgent) Equal(compared agt.AgentI) bool {
    casted, ok := compared.(*VoterAgent)

    if !ok {
        return false
    }

    if agent.id != casted.id {
        return false
    }

    if agent.url != casted.url {
        return false
    }

    return true
}

// DeepEqual Check if a VoterAgent is deep equal to another.
func (agent *VoterAgent) DeepEqual(compared agt.AgentI) bool {
    casted, ok := compared.(*VoterAgent)

    if !ok {
        return false
    }

    if agent.id != casted.id {
        return false
    }

    if agent.url != casted.url {
        return false
    }

    return true
}