package agt

import (
	"errors"
    "fmt"
    "ia04/comsoc"
    "reflect"
    "strconv"
	"time"
)

const NB_VOTERS = 5
const NB_ALTS = 10
const UTC_TIME_LAYOUT = "Mon Jan 2 15:04:05 UTC 2006"

type AgentID string

/*
Vote Object used internally to represent a Vote.
*/
type Vote struct {
	agentId AgentID
	voteId  string
	prefs   []comsoc.Alternative
	options []int
}

func NewVote(agentId AgentID, voteId string, prefs []comsoc.Alternative, options []int) *Vote {
	return &Vote{
		agentId: agentId,
		voteId:  voteId,
		prefs:   prefs,
		options: options,
	}
}

// String Convert a vote to string.
func (vote *Vote) String() string {
    return fmt.Sprintf("Vote{ agent-id=%s, vote-id=%s, prefs=%v, options=%v }", vote.agentId, vote.voteId, vote.prefs, vote.options)
}

// Equal Check if a vote is equal to another.
func (vote *Vote) Equal(compared Vote) bool {

    if vote.agentId != compared.agentId {
        return false
    }

    if vote.voteId != compared.voteId {
        return false
    }

    if !reflect.DeepEqual(vote.prefs, compared.prefs) {
        return false
    }

    if !reflect.DeepEqual(vote.options, compared.options) {
        return false
    }

    return true
}

// Clone Clone a Vote.
func (vote *Vote) Clone() Vote {
    return *vote
}

/*
Ballot Object used internally to represent a Ballot.
*/
type Ballot struct {
	ballotId string
	rule     string
	deadline time.Time
	voterIds []AgentID
	alts     int
	votes    []*Vote
}

// NewBallot Create a new Ballot.
func NewBallot(ballotId string, rule string, deadline time.Time, voterIds []AgentID, alts int) *Ballot {

	votes := make([]*Vote, 0, len(voterIds))

	ballot := Ballot{
		ballotId: ballotId,
		rule:     rule,
		deadline: deadline,
		voterIds: voterIds,
		alts:     alts,
		votes:    votes,
	}

	return &ballot
}

// GetId Get ballot id.
func (ballot *Ballot) GetId() string {
	return ballot.ballotId
}

// GetRule Get ballot rule.
func (ballot *Ballot) GetRule() string {
	return ballot.rule
}

// GetDeadline Get ballot deadline.
func (ballot *Ballot) GetDeadline() time.Time {
	return ballot.deadline
}

// GetVoterIds Get ballot voter ids.
func (ballot *Ballot) GetVoterIds() []AgentID {
	return ballot.voterIds
}

// GetAlts Get ballot alts.
func (ballot *Ballot) GetAlts() int {
	return ballot.alts
}

// GetVotes Get all the votes submitted in the ballot.
func (ballot *Ballot) GetVotes() []*Vote {
	return ballot.votes
}

// IsFinished Check if the ballot is finished.
func (ballot *Ballot) IsFinished() bool {
	return time.Now().After(ballot.deadline)
}

// CanVote Check if an agent can vote.
func (ballot *Ballot) CanVote(agent AgentID) bool {
	_, ok := Contains(ballot.voterIds, agent)
	return ok
}

// HasVoted Check if an agent has already voted.
func (ballot *Ballot) HasVoted(agent AgentID) bool {

	for _, vote := range ballot.votes {

		if vote.agentId == agent {
			return true
		}
	}
	return false
}

// IsValid Check that a submitted vote is valid.
func (ballot *Ballot) IsValid(vote *Vote) bool {

	// Check that all alternatives are valid.
	for _, alt := range vote.prefs {

		if alt < 1 || int(alt) > ballot.alts {
			return false
		}
	}

	// Check that no alternative is missing.
	if len(vote.prefs) != int(ballot.alts) {
		return false
	}

	// Check that no alternative is duplicated.
	if HasDuplicates(vote.prefs) {
		return false
	}

	return true
}

// AddVote Add a new vote.
func (ballot *Ballot) AddVote(vote *Vote) error {

	if !ballot.CanVote(vote.agentId) {
		return errors.New("agent cannot vote")
	}

	if ballot.HasVoted(vote.agentId) {
		return errors.New("agent has already voted")
	}

	if ballot.IsFinished() {
		return errors.New("vote is finished")
	}

	if !ballot.IsValid(vote) {
		return errors.New("vote is invalid")
	}

	ballot.votes = append(ballot.votes, vote)

	return nil
}

// GetProfile Generate a profile from a ballot.
func (ballot *Ballot) GetProfile() comsoc.Profile {

	profile := make(comsoc.Profile, len(ballot.votes))

	for i := 0; i < len(ballot.votes); i++ {
		profile[i] = ballot.votes[i].prefs
	}

	return profile
}

// GetThresholds Generate thresholds from a ballot. A conversion from external
//(begins at 1) to internal (begins at 0) is applied. 
func (ballot *Ballot) GetThresholds() []int {

    thresholds := make([]int, len(ballot.votes))

    for i := 0; i < len(ballot.votes); i++ {

        vote := ballot.votes[i]
        var threshold int = 1

        if len(vote.options) >= 1 && vote.options[0] <= len(ballot.votes) {
            threshold = vote.options[0]
        }

        thresholds[i] = threshold-1
    }

    return thresholds
}

// GetResult Get the result of the ballot.
func (ballot *Ballot) GetResult() *BallotResult {

	f := GetRuleFunction(ballot.rule)
	result, err := f(ballot)

	if err != nil {
		return nil
	}

	return result
}

// IsExpired Check if a ballot is expired. A ballot has expired if it has been completed
// more than 30 seconds ago.
func (ballot *Ballot) IsExpired() bool {
	expiration := ballot.deadline.Add(30 * time.Second)
	return time.Now().After(expiration)
}

// String Convert a ballot to string.
func (ballot *Ballot) String() string {
    return fmt.Sprintf("Ballot{ ballot-id=%s, rule=%s, deadline=%s, voters=%v, alts=%d, votes=%v }",
        ballot.ballotId, ballot.rule, ballot.deadline.Format(UTC_TIME_LAYOUT), ballot.voterIds, ballot.alts, ballot.votes)
}

// Equal Check if a ballot is equal to another.
func (ballot *Ballot) Equal(compared Ballot) bool {

    if ballot.ballotId != compared.ballotId {
        return false
    }

    if ballot.rule != compared.rule {
        return false
    }

    if !ballot.deadline.Equal(compared.deadline) {
        return false
    }

    if !reflect.DeepEqual(ballot.voterIds, compared.voterIds) {
        return false
    }

    for i := 0; i < len(ballot.votes); i++ {

        // Comparing with pointers.
        if ballot.votes[i] != compared.votes[i] {
            return false
        }
    }

    return true
}

// DeepEqual Check if a ballot is deep equal to another.
func (ballot *Ballot) DeepEqual(compared Ballot) bool {

    if ballot.ballotId != compared.ballotId {
        return false
    }

    if ballot.rule != compared.rule {
        return false
    }

    if !ballot.deadline.Equal(compared.deadline) {
        return false
    }

    if !reflect.DeepEqual(ballot.voterIds, compared.voterIds) {
        return false
    }

    for i := 0; i < len(ballot.votes); i++ {

        // Comparing with pointers.
        if !ballot.votes[i].Equal(*compared.votes[i]) {
            return false
        }
    }

    return true
}

// Clone Clone a Ballot.
func (ballot *Ballot) Clone() Ballot {

    cloned := Ballot{}
    cloned.ballotId = ballot.ballotId
    cloned.rule = ballot.rule
    cloned.deadline = ballot.deadline
    cloned.voterIds = ballot.voterIds
    cloned.votes = make([]*Vote, len(ballot.votes))

    for i, vote := range ballot.votes {
        clonedVote := vote.Clone()
        cloned.votes[i] = &clonedVote
    }

    return cloned
}

/*
BallotHandler Internal object to handle many different ballots.
*/
type BallotHandler struct {
	ballots []*Ballot
    count int
}

// NewBallotHandler Create a new BallotHandler
func NewBallotHandler() *BallotHandler {
	ballots := make([]*Ballot, 0, 100)
	return &BallotHandler{ballots: ballots, count:1}
}

// AddBallot Add a new ballot to the handler if it doesn't exist.
func (handler *BallotHandler) AddBallot(ballot *Ballot) {

	if !handler.HasBallot(ballot.GetId()) {
		handler.ballots = append(handler.ballots, ballot)
        handler.count++
	}
}

func (handler *BallotHandler) RemoveBallot(i int) {
    handler.ballots[i] = handler.ballots[len(handler.ballots) - 1]
    handler.ballots = handler.ballots[:len(handler.ballots) - 1]
}

// HasBallot Check if a ballot exists.
func (handler *BallotHandler) HasBallot(id string) bool {

	for _, ballot := range handler.ballots {

		if ballot.ballotId == id {
			return true
		}
	}

	return false
}

// GetBallot Get a ballot by id.
func (handler *BallotHandler) GetBallot(id string) (int, *Ballot) {

	for i, ballot := range handler.ballots {

		if ballot.ballotId == id {
			return i, ballot
		}
	}

	return -1, nil
}

// NextId Get the next available id for a ballot.
func (handler *BallotHandler) NextId() string {
	return "vote" + strconv.Itoa(handler.count)
}

// GetBallots Get all registered ballots.
func (handler *BallotHandler) GetBallots() []*Ballot {
	return handler.ballots
}

// CountBallots Count the registered ballots.
func (handler *BallotHandler) CountBallots() int {
	return len(handler.ballots)
}

// String Convert a BallotHandler to string.
func (handler *BallotHandler) String() string {
    return fmt.Sprintf("BallotHandler{ ballots=%v }", handler.ballots)
}

// Equal Check if a BallotHandler is equal to another.
func (handler *BallotHandler) Equal(compared BallotHandler) bool {

    if handler.count != compared.count {
        return false
    }

    for i := 0; i < len(compared.ballots); i++ {

        if !handler.ballots[i].Equal(*compared.ballots[i]) {
            return false
        }
    }

    return true
}

// DeepEqual Check if a BallotHandler is equal deep to another.
func (handler *BallotHandler) DeepEqual(compared BallotHandler) bool {

    if handler.count != compared.count {
        return false
    }

    for i := 0; i < len(compared.ballots); i++ {

        if !handler.ballots[i].DeepEqual(*compared.ballots[i]) {
            return false
        }
    }

    return true
}

// Clone Clone a BallotHandler.
func (handler *BallotHandler) Clone() BallotHandler {

    cloned := BallotHandler{}
    cloned.count = handler.count
    cloned.ballots = make([]*Ballot, len(handler.ballots))

    for i, ballot := range handler.ballots {
        clonedBallot := ballot.Clone()
        cloned.ballots[i] = &clonedBallot
    }

    return cloned
}

/*
BallotResult Object used in requests to represent the result of a ballot.
*/
type BallotResult struct {
	Winner  comsoc.Alternative   `json:"winner"`
	Ranking []comsoc.Alternative `json:"ranking,omitempty"`
}
