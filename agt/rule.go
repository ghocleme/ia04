package agt

import (
    "ia04/comsoc"
)

var Rules = map[string]func(*Ballot) (*BallotResult, error) {
    "majority": func(ballot *Ballot) (*BallotResult, error) {
        return getBallotResult(ballot, comsoc.MajoritySWF, comsoc.MajoritySCF)
    },
    "borda": func(ballot *Ballot) (*BallotResult, error) {
        return getBallotResult(ballot, comsoc.BordaSWF, comsoc.BordaSCF)
    },
    "copeland": func(ballot *Ballot) (*BallotResult, error) {
        return getBallotResult(ballot, comsoc.CopelandSWF, comsoc.CopelandSCF)
    },
    "approval": func(ballot *Ballot) (*BallotResult, error) {

        profile := ballot.GetProfile()
        tiebreak := getTiebreak(ballot)
        thresholds := ballot.GetThresholds()
        alts, err := comsoc.ApprovalSCF(profile, thresholds)

        if err != nil {
            return nil, err
        }

        winner, err := tiebreak(alts)

        if err != nil {
            return nil, err
        }

        return &BallotResult{Winner: winner, Ranking: []comsoc.Alternative{}}, nil
    },
    "stv": func(ballot *Ballot) (*BallotResult, error) {

        profile := ballot.GetProfile()
        order := getStrictOrder(ballot)
        tiebreak := getTiebreak(ballot)
        alts, err := comsoc.STV_SCF(profile, order)

        if err != nil {
            return nil, err
        }

        winner, err := tiebreak(alts)

        if err != nil {
            return nil, err
        }

        return &BallotResult{Winner: winner, Ranking: []comsoc.Alternative{}}, nil
    },
}

func getTiebreak(ballot *Ballot) func([]comsoc.Alternative) (comsoc.Alternative, error) {
    order := getStrictOrder(ballot)
    return comsoc.TieBreakFactory(order)
}

func getStrictOrder(ballot *Ballot) []comsoc.Alternative {

    order := make([]comsoc.Alternative, ballot.GetAlts())

    for i := 0; i < len(order); i++ {
        order[i] = comsoc.Alternative(i+1)
    }

    return order
}

func getBallotResult(ballot *Ballot, swf func(profile comsoc.Profile) (comsoc.Count, error), scf func (comsoc.Profile) ([]comsoc.Alternative, error)) (*BallotResult, error) {

    tiebreak := getTiebreak(ballot)

    f1 := comsoc.SWFFactory(swf, tiebreak)
    f2 := comsoc.SCFFactory(scf, tiebreak)

    profile := ballot.GetProfile()

    ranking, err1 := f1(profile)
    winner, err2 := f2(profile)

    if err1 != nil {
        return nil, err1
    }

    if err2 != nil {
        return nil, err2
    }

    return &BallotResult{Winner: winner, Ranking: ranking}, nil
}


// RuleExists Check if a rule exists.
func RuleExists(rule string) bool {

    for key, _ := range Rules {

        if key == rule {
            return true
        }
    }
    return false
}


// GetRuleFunction Get the function of result associated to a rule.
func GetRuleFunction(rule string) func(*Ballot) (*BallotResult, error) {

    for key, val := range Rules {

        if key == rule {
            return val
        }
    }
    return nil
}

func GetRandomRule() string {

    for k, _ := range Rules {
        return k
    }

    return "" // Cannot happen.
}