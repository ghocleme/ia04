package comsoc

/*
	STV SWF
*/
func STV_SWF(profile Profile, order []Alternative) (Count, error) {

	err := CheckProfile(profile)

	if err != nil {
		return nil, err
	}

	// The least preferred alternative is the most preferred when reversing the order.
	// This property enables to use the same tiebreak function but to find the least preferred alternative.
	reversed := Reverse(order)
	tiebreakLeast := TieBreakFactory(reversed)

	count := make(Count)
	ignored := make([]Alternative, 0)

	for i := 0; i < len(profile[0]); i++ {

		tempCount := make(Count)

		/*
			Only initializing a number for the alternatives not ignored.
		*/
		for _, alt := range profile[0] {
			
			if Rank(alt, ignored) == -1 {
				tempCount[alt] = 0
			}
		}

		/*
			Computing the majority by ignoring alternatives.
		*/
		for _, prefs := range profile {

			prefered := GetPreferred(prefs, ignored)

			tempCount[prefered]++
		}

		/*
			Checking if there is an absolute majority.
		*/
		max := MaxAmount(tempCount)

		if max > len(profile) / 2 {
			
			// For the remaining alternatives (not ignored), using the current
			// tour except for the winner.
			for _, alt := range profile[0] {

				if Rank(alt, ignored) != -1 { 
					continue 
				}

				if tempCount[alt] == max {
					count[alt] = i+1 // Winner.
				} else {
					count[alt] = i // Remaining alternative.
				}
			}

			return count, nil // Exit.
		}

		// Finding the least prefered.
		minAlts := MinCount(tempCount)
		minAlt, err := tiebreakLeast(minAlts)

		if err != nil {
			return nil, err
		}

		count[minAlt] = i

		// Adding it to ignored alternatives.
		ignored = append(ignored, minAlt)
	}

	return count, nil
}

/*
	STV SCF
*/
func STV_SCF(profile Profile, order []Alternative) ([]Alternative, error) {

	count, err := STV_SWF(profile, order)
	
	if err != nil  {
		return nil, err
	}

	return MaxCount(count), nil
}