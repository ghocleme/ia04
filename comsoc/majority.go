package comsoc

/*
	Majority simple SWF
*/
func MajoritySWF(profile Profile) (Count, error) {

	err := CheckProfile(profile)

	// Invalid profile.
	if err != nil {
		return nil, err
	}

	count := InitCount(profile[0])

	// Counting.
	for _, prefs := range profile {
		count[prefs[0]]++
	}

	return count, nil
}

/*
	Majority simple SCF
*/
func MajoritySCF(profile Profile) (bestAlts []Alternative, err error) {
	
	count, err := MajoritySWF(profile)
	
	if err != nil  {
		return
	}

	bestAlts = MaxCount(count)

	return
}