package comsoc

/*
	Copeland SWF
*/
func CopelandSWF(profile Profile) (Count, error) {

	err := CheckProfile(profile)

	if err != nil {
		return nil, err
	}

	// Initialization.
	count := make(Count)
	subsets := GetSubSets(profile[0])

	// Condorcet Winner computation.
	for _, subset := range subsets {

		var v1, v2 int

		for _, prefs := range profile {

			// Count the number of votant for each alternative of the subset.
			if IsPref(subset[0], subset[1], prefs) {
				v1++
			} else {
				v2++
			}
		}

		// Checking the alternative that wins the duel and incrementing
		if v1 > v2 {
			// Alt 1 won.
			count[subset[0]]++
			count[subset[1]]--
		} else if v1 < v2 {
			// Alt 2 won.
			count[subset[1]]++
			count[subset[0]]--
		}
	}

	return count, nil
}

/*
	Copeland SCF
*/
func CopelandSCF(profile Profile) ([]Alternative, error) {

	count, err := CopelandSWF(profile)
	
	if err != nil  {
		return nil, err
	}

	return MaxCount(count), nil
}