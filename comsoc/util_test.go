package comsoc

import "testing"

func TestRank(t *testing.T) {

	alts := []Alternative{1,2,3,4}

	res1 := Rank(1, alts)

	if res1 != 0 {
		t.Errorf("error, result for 1 should be 0, computed %d", res1)
	}

	res2 := Rank(4, alts)

	if res2 != 3 {
		t.Errorf("error, result for 4 should be 3, computed %d", res2)
	}

	res3 := Rank(50, alts)

	if res3 != -1 {
		t.Errorf("error, result for 50 should be -1, computed %d", res3)
	}
}

func TestMaxCount(t *testing.T) {

	count1 := Count{
		1:2,
		2:1,
		3:1,
	}

	alts1 := MaxCount(count1)

	if len(alts1) != 1 || alts1[0] != 1 {
		t.Errorf("error, 1 should be the only alternative with a count of 1, computed %v", count1)
	}

	count2 := Count{
		1: 3,
		2: 2,
		3: 1,
		4: 3,
		5: 0,
	}

	alts2 := MaxCount(count2)

	if len(alts2) != 2 {
		t.Errorf("error, 1 and 4 should be the best alternatives with a count of 3, computed %v", alts2)
	}
}

func TestCheckProfile(t *testing.T) {

	p1 := make([][]Alternative, 3)
	p1[0] = []Alternative{0,1,2}
	p1[1] = []Alternative{1,0,2}
	p1[2] = []Alternative{1,2,0}

	err1 := CheckProfile(p1)

	if err1 != nil {
		t.Errorf("error, profile is correct, computed %v", err1)
	}

	p2 := make([][]Alternative, 0)

	err2 := CheckProfile(p2)

	if err2 == nil {
		t.Errorf("error, empty profile not detected")
	}

	p3 := make([][]Alternative, 2)
	p3[0] = []Alternative{0,1,2}
	p3[1] = []Alternative{1,0,2,3}

	err3 := CheckProfile(p3)

	if err3 == nil {
		t.Errorf("error, preferences different size not detected")
	}

	p4 := make([][]Alternative, 2)
	p4[0] = []Alternative{0,1,2}
	p4[1] = []Alternative{1,0,1}

	err4 := CheckProfile(p3)

	if err4 == nil {
		t.Errorf("error, preferences with duplicate alternatives not detected")
	}
}

func TestGetSubset(t *testing.T) {

	res1 := GetSubSets([]Alternative{1,2,3})

	if len(res1) != 3 {
		t.Errorf("error, invalid number of subsets which must be 3, computed %v", len(res1))
	}

	res2 := GetSubSets([]Alternative{1,2,3,4})

	if len(res2) != 6 {
		t.Errorf("error, invalid number of subsets which must be 6, computed %v", len(res2))
	}
}