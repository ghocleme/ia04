package comsoc

/*
	Approval SWF
*/
func ApprovalSWF(profile Profile, thresholds []int) (Count, error) {

	err := CheckProfile(profile)

	if err != nil {
		return nil, err
	}

	// Initialization.
	count := InitCount(profile[0])

	// Counting
	for i, prefs := range profile {

		var threshold int = thresholds[i]

		for j := 0; j < threshold; j++ {

			count[prefs[j]]++
		}
	}

	return count, nil
}

/*
	Approval SCF
*/
func ApprovalSCF(profile Profile, thresholds []int) ([]Alternative, error) {

	count, err := ApprovalSWF(profile, thresholds)
	
	if err != nil  {
		return nil, err
	}

	bestAlts := MaxCount(count)

	return bestAlts, nil
}