package comsoc

import ("errors")

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int

/*
	Get the index of an alternative in an order or -1 if it is not found.
*/
func Rank(alternative Alternative, prefs []Alternative) int {

	for i, alt := range prefs {

		if alternative == alt {
			return i
		}
	}

	return -1
}

/*
	Return true if alt1 is preferred over alt2 or false otherwise.
*/
func IsPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	return Rank(alt1, prefs) <= Rank(alt2, prefs)
}

/*
	Get the best alternative given a count.
*/
func MaxCount(count Count) (bestAlts []Alternative) {

	max := MaxAmount(count)

	for alt, amount := range count {

		if amount == max {
			bestAlts = append(bestAlts, alt)
		}
	}
	return
}

/*
	Get the highest number associated to an alternative.
*/
func MaxAmount(count Count) int {

	var maxAmount int

	for v := range count {

		if count[v] > maxAmount {
			maxAmount = count[v]
		}
	}

	return maxAmount
}

/*
	Get the least alternative given a count.
*/
func MinCount(count Count) (leastAlts []Alternative) {

	min := MinAmount(count)

	for alt, amount := range count {

		if amount == min {
			leastAlts = append(leastAlts, alt)
		}
	}
	return
}

/*
	Get the lowest number associated to an alternative.
*/
func MinAmount(count Count) int {

	// Initialization needed because otherwise, 0 will
	// be taken as a minimum which is an unwanted behavior.
	var minAmount int = -1

	for v := range count {

		if count[v] < minAmount || minAmount == -1 {
			minAmount = count[v]
		}
	}

	return minAmount
}

/*
	Check if a given profile is valid :
	- All the orders have the same size.
	- Each alternative appears only one time in each order.
*/
func CheckProfile(profile Profile) error {

	if len(profile) == 0 {
		return errors.New("empty profile")
	}

	var size int = len(profile[0])

	for _, prefs := range profile {

		if len(prefs) != size {
			return errors.New("invalid preference order size")
		}

		duplError := CheckDuplicateAlts(prefs)

		if duplError != nil {
			return duplError
		}
	}

	return nil
}

/*
	Check if an alternative is duplicated in an order.
*/
func CheckDuplicateAlts(alts []Alternative) error {

	for i := 0; i < len(alts); i++ {

		for j := i+1; j < len(alts); j++ {

			if alts[i] == alts[j] {
				return errors.New("duplicate alternative")
			}
		}
 	}

	return nil
}

/*
	Initialize a count of alternatives.
*/
func InitCount(alts []Alternative) Count {

	count := make(map[Alternative]int)

	// Intialization.
	for _, alt := range alts {
		count[alt] = 0
	}

	return count
}

/*
	Get all the subsets of two elements which are possible to
	obtain given a set of alternatives (2 among n).
*/
func GetSubSets(alts []Alternative) [][]Alternative {

	subsets := make([][]Alternative, 0, len(alts))

	for i := 0; i < len(alts)-1; i++ {

		for j := i+1; j < len(alts); j++ {

			subset := []Alternative{alts[i], alts[j]}
			subsets = append(subsets, subset)
		}
	}

	return subsets
}

/*
	Get the preferred alternative of an order by ignoring some others.
*/
func GetPreferred(prefs []Alternative, ignored []Alternative) Alternative {

	for _, alt := range prefs {

		if Rank(alt, ignored) == -1 {
			return alt
		}
	}

	return -1
}

/*
	Create a reversed copy of a slice.
*/
func Reverse[T any](slice []T) []T {

	copy := make([]T, len(slice))

	for i := 0; i < len(slice); i++ {
		copy[i] = slice[len(slice)-i-1]
	}

	return copy
}