package comsoc

import "testing"

func TestBordaSWF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, _ := BordaSWF(prefs)

	if res[1] != 4 {
		t.Errorf("error, result for 1 should be 4, %d computed", res[1])
	}
	if res[2] != 3 {
		t.Errorf("error, result for 2 should be 3, %d computed", res[2])
	}
	if res[3] != 2 {
		t.Errorf("error, result for 3 should be 2, %d computed", res[3])
	}
}

func TestBordaSCF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, err := BordaSCF(prefs)

	if err != nil {
		t.Error(err)
	}

	if len(res) != 1 || res[0] != 1 {
		t.Errorf("error, 1 should be the only best alternative, computed %v", res)
	}

	prefs2 := BuildTestProfile()
	res2, err2 := BordaSCF(prefs2)

	if err2 != nil {
		t.Error(err2)
	}

	if len(res2) != 1 || res2[0] != 2 {
		t.Errorf("error, 4 should be the only best alternative, computed %v", res2)
	}
}

func TestMajoritySWF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, _ := MajoritySWF(prefs)

	if res[1] != 2 {
		t.Errorf("error, result for 1 should be 2, %d computed", res[1])
	}
	if res[2] != 0 {
		t.Errorf("error, result for 2 should be 0, %d computed", res[2])
	}
	if res[3] != 1 {
		t.Errorf("error, result for 3 should be 1, %d computed", res[3])
	}
}

func TestMajoritySCF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res, err := MajoritySCF(prefs)

	if err != nil {
		t.Error(err)
	}

	if len(res) != 1 || res[0] != 1 {
		t.Errorf("error, 1 should be the only best alternative, computed %v", res)
	}

	prefs2 := BuildTestProfile()
	res2, err2 := MajoritySCF(prefs2)

	if err2 != nil {
		t.Error(err2)
	}

	if len(res2) != 1 || res2[0] != 4 {
		t.Errorf("error, 4 should be the only best alternative, computed %v", res2)
	}
}

func TestApprovalSWF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 2, 3},
		{1, 3, 2},
		{2, 3, 1},
	}

	thresholds := []int{2, 1, 2}

	res, _ := ApprovalSWF(prefs, thresholds)

	if res[1] != 2 {
		t.Errorf("error, result for 1 should be 2, %d computed", res[1])
	}
	if res[2] != 2 {
		t.Errorf("error, result for 2 should be 2, %d computed", res[2])
	}
	if res[3] != 1 {
		t.Errorf("error, result for 3 should be 1, %d computed", res[3])
	}

	thresholds2 := []int{1,0,0}
	res2, _ := ApprovalSWF(prefs, thresholds2)

	if res2[1] != 1 {
		t.Errorf("error, result for 1 should be 1, %d computed", res2[1])
	}

	if res2[2] != 0 {
		t.Errorf("error, result for 2 should be 0, %d computed", res2[2])
	}

	if res2[3] != 0 {
		t.Errorf("error, result for 3 should be 0, %d computed", res2[3])
	}
}

func TestApprovalSCF(t *testing.T) {
	prefs := [][]Alternative{
		{1, 3, 2},
		{1, 2, 3},
		{2, 1, 3},
	}

	thresholds := []int{2, 1, 2}

	res, err := ApprovalSCF(prefs, thresholds)

	if err != nil {
		t.Error(err)
	}

	if len(res) != 1 || res[0] != 1 {
		t.Errorf("error, 1 should be the only best alternative, computed %v", res)
	}
}

func TestCondorcetWinner(t *testing.T) {

	// Basic tests.
	prefs1 := [][]Alternative{
		{3, 2, 1},
		{1, 2, 3},
		{1, 2, 3},
	}

	prefs2 := [][]Alternative{
		{3, 1, 2},
		{1, 2, 3},
		{2, 3, 1},
	}

	res1, _ := CondorcetWinner(prefs1)
	res2, _ := CondorcetWinner(prefs2)

	if len(res1) == 0 || res1[0] != 1 {
		t.Errorf("error, 1 should be the only best alternative for prefs1, computed %v", res1)
	}

	if len(res2) != 0 {
		t.Errorf("error, no best alternative for prefs2, computed %v", res2)
	}

	// More complex example.
	prefs3 := BuildTestProfile()
	res3, err3 := CondorcetWinner(prefs3)

	if err3 != nil {
		t.Error(err3)
	}

	if len(res3) != 1 || res3[0] != 3 {
		t.Errorf("error, 2 should be the only best alternative, computed %v", res3)
	}

	// Case in which two alternatives have an equal number of votes.
	prefs4 := [][]Alternative{
		{1, 2, 3, 4},
		{1, 3, 2, 4},
		{2, 3, 1, 4},
		{2, 3, 4, 1},
	}

	res4, err4 := CondorcetWinner(prefs4)

	if err4 != nil {
		t.Error(err3)
	}

	if len(res4) != 0 {
		t.Errorf("error, no alternative should be found")
	}
}

func TestTieBreak(t *testing.T) {

	ord1 := []Alternative{1,2,3,4}
	tb1 := TieBreakFactory(ord1)

	// Test 1
	best1, err1 := tb1([]Alternative{2,1})

	if err1 != nil {
		t.Error(err1)
	}

	if best1 != 1 {
		t.Errorf("error, 1 should be the best alternative, computed %v", best1)
	}

	// Test 2
	best2, err2 := tb1([]Alternative{3})

	if err2 != nil {
		t.Error(err2)
	}

	if best2 != 3 {
		t.Errorf("error, 3 should be the best alternative, computed %v", best2)
	}

	// Test 2
	ord2 := []Alternative{1,3,4,2}
	tb2 := TieBreakFactory(ord2)

	best3, err3 := tb2([]Alternative{2,3,4,1})

	if err3 != nil {
		t.Error(err3)
	}

	if best3 != 1 {
		t.Errorf("error, 1 should be the best alternative, computed %v", best3)
	}
}

func TestBordaSWFFactory(t *testing.T) {

	tiebreak := TieBreakFactory([]Alternative{3,2,1})
	bordaTb := SWFFactory(BordaSWF, tiebreak)

	// Testing that the normal behavior is conserved when no equality.
	prefs1 := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res1, err1 := bordaTb(prefs1)

	if err1 != nil {
		t.Error(err1)
	}

	if len(res1) != 3 {
		t.Error("error, missing alternatives")
	}

	if res1[0] != 1 {
		t.Errorf("error, 1 should be the best, %d computed", res1[0])
	}

	if res1[1] != 2 {
		t.Errorf("error, 2 should be the second, %d computed", res1[1])
	}

	if res1[2] != 3 {
		t.Errorf("error, 3 should be third, %d computed", res1[2])
	}

	// Testing when there is an equality (between 1 and 2).
	prefs2 := [][]Alternative{
		{1, 2, 3},
		{2, 1, 3},
		{2, 1, 3},
	}

	res2, err2 := bordaTb(prefs2)

	if err2 != nil {
		t.Error(err2)
	}

	if len(res2) != 3 {
		t.Error("error, missing alternatives")
	}

	if res2[0] != 2 {
		t.Errorf("error, 1 should be the best, %d computed", res2[0])
	}

	if res2[1] != 1 {
		t.Errorf("error, 2 should be the second, %d computed", res2[1])
	}

	if res2[2] != 3 {
		t.Errorf("error, 3 should be third, %d computed", res2[2])
	}
}

func TestBordaSCFFactory(t *testing.T) {

	tiebreak := TieBreakFactory([]Alternative{3,2,1})
	bordaTb := SCFFactory(BordaSCF, tiebreak)

	// Testing that the normal behavior is conserved when no equality.
	prefs1 := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res1, err1 := bordaTb(prefs1)

	if err1 != nil {
		t.Error(err1)
	}

	if res1 != 1 {
		t.Errorf("error, 1 should be the best alternative, %d computed", res1)
	}

	// Testing when there is an equality (between 1 and 2).
	prefs2 := [][]Alternative{
		{1, 2, 3},
		{2, 1, 3},
		{2, 1, 3},
	}

	res2, err2 := bordaTb(prefs2)

	if err2 != nil {
		t.Error(err2)
	}

	if res2 != 2 {
		t.Errorf("error, 2 should be the best alternative, %d computed", res2)
	}
}

func TestMajoritySWFFactory(t *testing.T) {

	tiebreak := TieBreakFactory([]Alternative{3,2,1})
	majorityTb := SWFFactory(MajoritySWF, tiebreak)

	// Testing with a normal behavior.
	prefs1 := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}

	res1, err1 := majorityTb(prefs1)

	if err1 != nil {
		t.Error(err1)
	}

	if res1[0] != 1 {
		t.Errorf("error, alternative 1 should be the best, %d computed", res1[0])
	}

	if res1[1] != 3 {
		t.Errorf("error, altenative 1 should be the second, %d computed", res1[1])
	}

	if res1[2] != 2 {
		t.Errorf("error, alternative 2 should be the third, %d computed", res1[2])
	}

	// Testing when there is an equality (between 1 and 3).
	prefs2 := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
		{3, 1, 2},
	}

	res2, err2 := majorityTb(prefs2)

	if err2 != nil {
		t.Error(err2)
	}

	if res2[0] != 3 {
		t.Errorf("error, alternative 1 should be the best, %d computed", res2[0])
	}

	if res2[1] != 1 {
		t.Errorf("error, altenative 1 should be the second, %d computed", res2[1])
	}

	if res2[2] != 2 {
		t.Errorf("error, alternative 2 should be the third, %d computed", res2[2])
	}
}

func TestMajoritySCFFactory(t *testing.T) {

	tiebreak := TieBreakFactory([]Alternative{3,2,1})
	majorityTb := SCFFactory(MajoritySCF, tiebreak)

	// Testing with a normal behavior.
	prefs1 := [][]Alternative{
		{2, 1, 3},
		{2, 3, 1},
		{3, 2, 1},
	}

	res1, err1 := majorityTb(prefs1)

	if err1 != nil {
		t.Error(err1)
	}

	if res1 != 2 {
		t.Errorf("error, 2 should be the best alternative, computed %v", res1)
	}

	// Testing when there is an equality.
	prefs2 := [][]Alternative{
		{2, 1, 3},
		{1, 3, 2},
		{3, 2, 1},
	}

	res2, err2 := majorityTb(prefs2)

	if err2 != nil {
		t.Error(err1)
	}

	if res2 != 3 {
		t.Errorf("error, 3 should be the best alternative, computed %v", res2)
	}
}

func TestCopelandSWF(t *testing.T) {

	prefs := BuildCopelandProfile()

	count, err := CopelandSWF(prefs)

	if err != nil {
		t.Error(err)
	}

	if count[1] != -1 {
		t.Errorf("error, 1 should have a score of -1, computed %v", count[1])
	}

	if count[2] != 1 {
		t.Errorf("error, 2 should have a score of 1, computed %v", count[2])
	}

	if count[3] != 1 {
		t.Errorf("error, 3 should have a score of 1, computed %v", count[3])
	}

	if count[4] != -1 {
		t.Errorf("error, 4 should have a score of -1, computed %v", count[4])
	}
}

func TestCopelandSCF(t *testing.T) {

	prefs := BuildCopelandProfile()

	alts, err := CopelandSCF(prefs)

	if err != nil {
		t.Error(err)
	}

	if len(alts) != 2 || Rank(2, alts) == -1 || Rank(3, alts) == -1 {
		t.Errorf("error, best alternatives must be 1 and 4, computed %v", alts)
	}
}

func TestSTVSWF(t *testing.T) {

	// Test without early majority.
	prefs := [][]Alternative{
		{3,1,2,4},
		{3,1,2,4},
		{4,3,2,1},
		{1,4,3,2},
		{1,4,3,2},
	}

	count, err := STV_SWF(prefs, []Alternative{1,2,3,4})

	if err != nil {
		t.Error(err)
	}

	if len(count) != 4 {
		t.Errorf("error, 4 alternatives should be present, computed %v", len(count))
	}

	if count[1] != 2 {
		t.Errorf("error, number for alternative 1 should be 2, computed %v", count[1])
	}

	if count[2] != 0 {
		t.Errorf("error, number for alternative 2 should be 0, computed %v", count[2])
	}

	if count[3] != 3 {
		t.Errorf("error, number for alternative 3 should be 3, computed %v", count[3])
	}

	if count[4] != 1 {
		t.Errorf("error, number for alternative 4 should be 1, computed %v", count[4])
	}

	// Test with a majority.
	prefs2 := [][]Alternative{
		{3,1,2,4},
		{3,1,2,4},
		{3,4,2,1},
		{4,3,2,1},
		{1,4,3,2},
	}

	count2, err2 := STV_SWF(prefs2, []Alternative{1,2,3,4})

	if err2 != nil {
		t.Error(err2)
	}

	if count2[1] != 0 {
		t.Errorf("error, number for alternative 1 should be 0, computed %v", count2[1])
	}

	if count2[2] != 0 {
		t.Errorf("error, number for alternative 2 should be 0, computed %v", count2[2])
	}

	if count2[3] != 1 {
		t.Errorf("error, number for alternative 3 should be 1, computed %v", count2[3])
	}

	if count2[4] != 0 {
		t.Errorf("error, number for alternative 4 should be 0, computed %v", count2[4])
	}
}

func BuildTestProfile() Profile {

	prefs := make([][]Alternative, 0)

	for i := 0; i < 5; i++ {
		prefs = append(prefs, []Alternative{1,2,3,4})
	}

	for i := 0; i < 4; i++ {
		prefs = append(prefs, []Alternative{1,3,2,4})
	}

	for i := 0; i < 2; i++ {
		prefs = append(prefs, []Alternative{4,2,1,3})
	}

	for i := 0; i < 6; i++ {
		prefs = append(prefs, []Alternative{4,2,3,1})
	}

	for i := 0; i < 8; i++ {
		prefs = append(prefs, []Alternative{3,2,1,4})
	}

	for i := 0; i < 2; i++ {
		prefs = append(prefs, []Alternative{4,3,2,1})
	}

	return prefs
}

func BuildCopelandProfile() Profile {

	prefs := make([][]Alternative, 0)

	for i := 0; i < 5; i++ {
		prefs = append(prefs, []Alternative{1,2,3,4})
	}

	for i := 0; i < 4; i++ {
		prefs = append(prefs, []Alternative{2,3,4,1})
	}

	for i := 0; i < 3; i++ {
		prefs = append(prefs, []Alternative{4,3,1,2})
	}

	return prefs
}