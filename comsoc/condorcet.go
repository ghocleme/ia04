package comsoc

/*
	Condorcet Winner.
*/
func CondorcetWinner(profile Profile) ([]Alternative, error) {

	err := CheckProfile(profile)

	if err != nil {
		return nil, err
	}

	// Initialization.
	count := make(Count)
	res := make([]Alternative, 0)
	subsets := GetSubSets(profile[0])

	// Condorcet Winner computation.
	for _, subset := range subsets {

		var v1, v2 int

		for _, prefs := range profile {

			if IsPref(subset[0], subset[1], prefs) {
				v1++
			} else {
				v2++
			}
		}

		// Checking the alternative that wins the duel and incrementing
		if v1 > v2 {
			// Alt 1 won.
			count[subset[0]]++
		} else if v1 < v2 {
			// Alt 2 won.
			count[subset[1]]++
		} else {
			// Equality => No Condorcet Winner.
			return res, nil
		}
	}

	maxcount := MaxCount(count)

	// If an alternative is a Condorcet Winner, it should have m-1 victories,
	// where m is the total number of alternatives.
	if len(maxcount) == 1 && count[maxcount[0]] == len(profile[0]) - 1 {
		return maxcount, nil
	}

	return res, nil
}