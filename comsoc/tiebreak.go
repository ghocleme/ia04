package comsoc

import "errors"

func TieBreakFactory(order []Alternative) func([]Alternative) (Alternative, error) {

	return func(alts []Alternative) (Alternative, error) {

		if len(alts) == 0 {
			return -1, errors.New("error, no alternative provided")
		}

		// Finding the best alternative according to the order.
		var best Alternative = alts[0]

		for i := 1; i < len(alts); i++ {

			if IsPref(alts[i], best, order) {
				best = alts[i]
			}
		}

		return best, nil
	}
}

func SWFFactory(swf func (Profile) (Count, error), tiebreak func ([]Alternative) (Alternative, error)) (func(Profile) ([]Alternative, error)) {
	
	return func(profile Profile) ([]Alternative, error) {

		count, err1 := swf(profile)

		if err1 != nil {
			return nil, err1
		}

		// A partir du count et de l'ordre stricte, renvoyer toutes les alternatives
		// correctement ordonnées (ne pas supprimer les égalités, les ordonner uniquement).

		ordered := make([]Alternative, 0)

		for len(count) != 0 {

			maxcount := MaxCount(count) // Meilleures alternatives.
			
			for len(maxcount) != 0 {

				best, _ := tiebreak(maxcount) // Meilleure des meilleures (si égalité).
				rank := Rank(best, maxcount)

				ordered = append(ordered, best)

				// Retirer la meilleure pour continuer sur la deuxième meilleure et ainsi de suite.
				maxcount = append(maxcount[:rank], maxcount[rank+1:]...)
				delete(count, best)
			}
		}

		return ordered, nil
	}
}

func SCFFactory(scf func (Profile) ([]Alternative, error), tiebreak func ([]Alternative) (Alternative, error)) (func(Profile) (Alternative, error)) {
	
	return func(profile Profile) (Alternative, error) {

		alts, err1 := scf(profile)

		if err1 != nil {
			return -1, err1
		}

		best, err2 := tiebreak(alts)

		if err2 != nil {
			return -1, err2
		}

		return best, nil
	}
}
