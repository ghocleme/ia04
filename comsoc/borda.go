package comsoc

/*
	Borda SWF
*/
func BordaSWF(profile Profile) (Count, error) {

	err := CheckProfile(profile)

	// Invalid profile.
	if err != nil {
		return nil, err
	}

	count := InitCount(profile[0])

	// Counting.
    for _, prefs := range profile {
        
        for i, v := range prefs {

            score := len(prefs)-i-1
            count[v] += score
        }
    }

	return count, nil
}

/*
	Borda SCF
*/
func BordaSCF(profile Profile) ([]Alternative, error) {

	count, err := BordaSWF(profile)

	if err != nil  {
		return nil, err
	}

	bestAlts := MaxCount(count)

	return bestAlts, nil
}