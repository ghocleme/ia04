# TP AI30

## Présentation générale

Ce projet a été réalisé par **Clément Gho** et **Ryad Aïdli** dans le cadre du TP de l'UV AI30. Il a pour objectif simuler un bureau de vote en ligne, avec une API REST, dans le langage Go.

## Installation

Pour installer le projet sur votre machine, vous pouvez le télécharger depuis GitLab:
```
https://gitlab.utc.fr/ghocleme/ia04
```

## Tester le projet

Pour tester le projet, il faut exécuter l'un après l'autre les fichiers `server-agent.go`, `sponsor-agent.go` et `voter-agent.go` du package `cmd`. Vous pouvez vous placer à la racine du dossier `ai04` et exécuter les commandes suivantes:

Lancement du **serveur**:
```
go run cmd/server/server-agent.go
```

Lancement des **commanditaires**:
```
go run cmd/sponsor/sponsor-agent.go
```

Lancement des **votants**:
```
go run cmd/client/voter-agent.go
```

## Fonctionnalités

Le bureau de vote que nous avons mis en place est totalement automatisé. Cela veut dire que la gestion du cycle de vie des scrutins (création, suppression), des votes (soumission) et de la récupération des résultats se fait automatiquement, sans intervention humaine. Pour cela, nous avons implémenté 3 types d'agents qui sont détaillés ci-dessous.

### BallotAgent (serveur)

- Implémentation : `agt/ballotagent/ballotagent.go`
- Lancement : `cmd/server/server-agent.go`

Agent qui joue le rôle de serveur. Il réceptionne toutes les requêtes effectuées par les autres types d'agent et stocke les résultats en mémoire.

### SponsorAgent (commanditaire)

- Implémentation : `agt/sponsoragent/sponsoragent.go`
- Lancement : `cmd/sponsor/sponsor-agent.go`

Ce type d'agent joue le rôle de commanditaire. Il gère l'ensemble du cycle de vie des scutins. Une fois le programme lancé, un nouvel agent responsable d'un unique scrutin est créé toutes les 15 secondes. Son cycle de vie est décrit ci-dessous:

(1) Création d'un nouveau scutin (commande `/new_ballot`)

- Une règle de vote est choisie aléatoirement parmi celle disponible.
- Les votants ayant la possibilité de voter dans le scutin sont générés aléatoirement et sont définis sur la plage [ad_id1 ; ag_id10]
- La durée d'un scrutin est de 30 secondes.
- Le nombre d'alternatives possibles est toujours de 10.

(2) Récupération des résultats une fois le scrutin terminé et affichage (commande `/result`).

(3) Suppression du scrutin une fois celui-ci expiré, c'est-à-dire 15 secondes après récupération des résultats (commande `/del_ballot`).

(4) Suppression de l'agent.

### VoterAgent (votant)

- Implémentation : `agt/voteragent/voteragent.go`
- Lancement : `cmd/client/voter-agent.go`

Ce type d'agent joue le rôle de votant. Au lancement du programme, 10 agents sont créés. Toutes les 5 secondes, chaque agent récupère les scutins auxquels il a accès (commande `/ballots`). Il sélectionne ensuite le premier scrutin, s'il existe, pour lequel il n'a pas déjà voté et soumet son vote (commande `/vote`). Les préférences et options de chaque agent sont définis aléatoirement, pour chaque requête. Ce choix a été fait pour, d'une part, obtenir des résultats de scrutin très différents et, d'autre part, faciliter l'implémentation.

### Règles de vote

Les règles de vote **implémentées** et **disponibles** dans le programme sont les suivantes :
- `Majorité simple`
- `Borda`
- `Condorcet`
- `Copeland`
- `Approbation`
- `STV`

Les règles de vote **disponibles** dans le **bureau de vote** sont les suivantes:
- `majority` (winner: oui, ranking: oui)
- `borda` (winner: oui, ranking: oui)
- `copeland` (winner: oui, ranking: oui)
- `approval` (winner: oui, ranking: non)
- `stv` (winner: oui, ranking: non)

## Exemples de fonctionnement

### Serveur

![server](resources/img/server-agent.png)

### Commanditaire

![sponsor](resources/img/sponsor-agent.png)

### Votants (clients)

![voter](resources/img/voter-agent.png)

## API REST

L'API REST demandée dans le cadre de ce TP a été totalement implémentée. En plus de celle-ci, des fonctionnalités supplémentaires ont été développées.

Remarque 1 : Il est à noter que notre API REST n'est pas totalement Restful car elle n'est pas sans état (stateless). Cependant, ce choix technique était nécessaire pour ce projet, qui necéssite la conservation de certaines données côté serveur.

Remarque 2 : L'ensemble des tests liés à l'API REST sont disponibles dans le dossier `resources/api-test`.

### Commande `/ballots`

- Objectif : Permet à un agent votant de récupérer les votes auquels il a accès.
- Requête : `POST`


- Objet `JSON` envoyé

| Propriété  | Type     | Signification         | Exemple de valeurs possibles |
|------------|----------|-----------------------|------------------------------|
| `agent-id` | `string` | Identifiant du votant | `ag_id1`                     |

- Code retour possibles

| Code retour | Signification      |
|-------------|--------------------|
| `200`       | ballots retrieved  |
| `405`       | method not allowed |
| `400`       | bad request        |

- Objet `JSON` renvoyé (si code retour = `200`)

Un tableau de scrutins est renvoyé. Chaque scrutin possède les propriétés suivantes:

| Propriété   | Type      | Signification                                    | Exemple de valeurs possibles     |
|-------------|-----------|--------------------------------------------------|:---------------------------------|
| `ballot-id` | `string`  | Identifiant du scrutin                           | `"vote1"`, `"vote2"`, ...        |
| `rule`      | `string`  | Règle de vote du scrutin                         | Voir section `Rules`             |
| `deadline`  | `string`  | Deadline du scrutin                              | `"Tue Nov 10 23:00:00 UTC 2009"` |
| `#alts`     | `int`     | Alternatives possibles                           | `10`                             |
| `has-voted` | `boolean` | Indique si le votant a déjà voté pour ce scrutin | `true`, `false`                  |

### Commande `/del_ballot`

- Objectif: Permet de supprimer un scrutin existant.
- Requête : `POST`


- Objet `JSON` envoyé

| Propriété   | Type     | Signification         | Exemple de valeurs possibles |
|-------------|----------|-----------------------|------------------------------|
| `ballot-id` | `string` | Identifiant du scutin | `vote1`                      |


- Code retour possibles

| Code retour | Signification       |
|-------------|---------------------|
| `200`       | ballot deleted      |
| `405`       | method not allowed  |
| `400`       | bad request         |
| `404`       | ballot not found    |
| `425`       | ballot not finished |